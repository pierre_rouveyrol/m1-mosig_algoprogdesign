/*Author : Pierre Rouveyrol <Pierre.Rouveyrol@e.ujf-grenoble.fr><rouveyrolpierre@gmail.com>
I used some function found on the internet to calculate the number of set bits in an int efficiently.
Compile with c99 and -lm for the math library.*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int countSetBits(int v) //Brian Kernighan's way to count set bits in an int.
{
    int c;

    for(c=0; v; c++)
        v &= v-1;

    return c;
}

int main(int argc, char*argv[]) 
{
    if (argc != 2) //input check
    {
        printf("You must enter N (and only N) as an argument\n");
        exit(1);
    }
   
    int n = atoi(argv[1]);
    int endCondition = (int)pow(2,n)-1;
    int switches = 0;

    for(int i=0; i<endCondition; i++)
    {
        switches += countSetBits(i^i+1); //count the number of switches for each increment
    }

    printf("%d\n",switches);
}

