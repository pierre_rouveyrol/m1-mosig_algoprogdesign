/*this is the first implementation of the Dwarf simulator
 * see http://www.paroles-musique.com/imprim.php?id=69967 if willing to lose time
 * V and W are considered integers*/

#include <stdlib.h>
#include <stdio.h>
#include "sglib.h"
#define ILIST_COMPARATOR(e1, e2) (e2->ratio - e1->ratio)
#define ILIST_COMPARATOR2(e1, e2) (e1->i - e2->i)

typedef struct availableItems //struct for list of available items
{
    int v; //value of item
    int w; //weight of item
    int i; //item number/id
    double ratio; //val/wei ratio
    struct availableItems* next;
} availableItems;


typedef struct chosenItems //struct for list of chosen items
{
    int i; //id of item
    int n; //number chosen
    struct chosenItems* next;
} chosenItems;

void loadItems(char* filename, int* bagCap, availableItems* avIt){
    int numItem;
    FILE *fp;
    fp = fopen(filename, "r");

    fscanf(fp, "%d %d", bagCap, &numItem);

    for(int i=0; i<numItem; i++){
        fscanf(fp, "%d %d", &(avIt->v), &(avIt->w));
        avIt->i = i+1;
        avIt->ratio = (double)(avIt->v)/(double)(avIt->w);
        if(i != numItem-1){
            avIt->next = malloc(sizeof(availableItems));
            avIt = avIt->next;
        }
        else
            avIt = NULL;
    }
    fclose(fp);
}

int chooseItems(availableItems* inlist, chosenItems* outlist, int bagCap){
    int R = bagCap;
    chosenItems* head = outlist;
    while(inlist)
    {
        outlist->i = inlist->i;
        outlist->n = (int)(R/inlist->w);
        R = R-(int)(R/inlist->w)*inlist->w;
        inlist = inlist->next;
        if(inlist){
            outlist->next = malloc(sizeof(chosenItems));
            outlist = outlist->next;
        }
        else
            outlist->next = NULL;
    }
    outlist = head;
    return (bagCap-R);
}

int main(int argc, char** argv){
    availableItems* avIt = malloc(sizeof(availableItems));
    chosenItems* chIt = malloc(sizeof(chosenItems));
    int bagCap; //capacity of the bag
    char* filename;
    
    if (argc != 2){
        printf("Usage : greedy filename\n");
        exit(1);
    }
    else{
        filename = argv[1];    
    }

    loadItems(filename, &bagCap, avIt);
    SGLIB_LIST_SORT(struct availableItems, avIt, ILIST_COMPARATOR, next);//list mergesort using sglib
  
    int totalWeight = chooseItems(avIt, chIt ,bagCap);

    SGLIB_LIST_SORT(struct chosenItems, chIt, ILIST_COMPARATOR2, next);//reorder output list
    
    printf("total weight of chosen items : %d\n", totalWeight);
    while(chIt)
    {
        printf("%d ", chIt->n);
        chIt = chIt->next;
    }
    printf("\n");
}
